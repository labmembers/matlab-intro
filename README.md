# MATLAB Introductory Workshop

Welcome to the Decision Neuroscience Lab MATLAB Workshop! This workshop contains material and code to help you start using the MATLAB programming language. It introduces the basics of MATLAB and prepares you to write your own code. There are exercises and examples provided to help you along the way.

This workshop was created for the Decision Neuroscience Lab 2019 Offsite Day at the Abbotsford Convent. However you can complete this workshop without attending this workshop or visiting a convent of any kind. 

All workshop material is stored in the [wiki](https://bitbucket.org/labmembers/matlab-intro/wiki/Home) of this repository.

Written by Daniel Feuerriegel, March 2019 at the University of Melbourne. This workshop is covered under a Creative Commons [CC-By Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/). 


## What does this workshop cover?

In this workshop we covers the basics of using MATLAB. We will start from the basics, including using MATLAB as a calculator, and then will cover some different data types and basic functions along the way. We will do some basic statistical summaries (e.g., calculating the mean and median) and will make some simple plots of data. 

To access the workshop material, go to the [wiki](https://bitbucket.org/labmembers/matlab-intro/wiki/Home).

## Things to prepare before attending this workshop

To complete this workshop you will need to have MATLAB installed on your computer. MATLAB licenses are provided by the University of Melbourne (see [the UoM software page](http://unimelb.libguides.com/c.php?g=402784&p=2740903)). We will not have time during the hands-on session to install MATLAB, so make sure you get these before coming to the workshop. If you can't install MATLAB, sit next to a friend who has been able to install it on their laptop.